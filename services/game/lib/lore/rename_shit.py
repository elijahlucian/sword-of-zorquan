import os

def rename_files_in_directory(dir):

  for filename in os.listdir(dir):
    filename = os.path.join(dir, filename)
    new_filename = filename.replace(' ', '_').lower()
    if os.path.isdir(filename):
      os.rename(filename, new_filename)
      rename_files_in_directory(new_filename)
    os.rename(filename, new_filename)
    print(filename, '=>', new_filename)

rename_files_in_directory('.')