module Modules
	module Actions
		def story 
			# 
		end

		def choice
			print 'Make your choice >> '
			# get player choice
			player_choice = gets.chomp.split(' ') # methodize this
			key = player_choice[0]
			value = player_choice[1]
			# [look, go, speak, get, attack]

			case key
			when 'look'
				# list exits
				@current_game_tile.exits.each do |direction|
					p "There is an exit to the #{Components::GameTile.serial_directions[direction]}"
				end

			when 'go'
				exit_choice = Components::GameTile.directions[value.to_sym]
				origin = get_direction(exit_choice)
				
			when 'speak'
			when 'get'
			when 'attack'
			when 'exit'
				p 'your journey has ended...'
				exit
			else
				puts 'that is not an option'
			end

			# pp @current_game_tile

		end



		def get_direction(origin)
			if @current_game_tile.exits.include? origin
				@current_game_tile = Components::GameTile.new({direction_from: origin})
				@world_map << @current_game_tile
			else
				p "there is no exit in that direction"
			end

		end

	end
end