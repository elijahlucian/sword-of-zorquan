module Components
  class Loot
    include Modules::Value

    attr_accessor :item, :id

    def initialize(props)
      @id = rand(1..9999) # if not unique... do thing
      @stack_max = 64
      @item = props[:global_items][:items].sample
    end

  end
end