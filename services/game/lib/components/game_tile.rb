module Components
  class GameTile
    
    @@game_tile_count = 0

    attr_accessor :id, :exits, :npcs, :loot, :enemies

    def initialize(args = {})
      @@game_tile_count += 1	
      if args[:first_game_tile] == true
        start_game
      else
        @exits = generate_exits(args[:direction_from])
        @npcs = check_for_person
        @loot = generate_loot
        @enemies = generate_enemies
      end 
      
      @id = "gametile.#{@@game_tile_count.to_i}"

    end

    def start_game
      
      @npcs = Components::Person.new({first_game_tile: true})
      @exits = [0]
      @enemies = []
    end

    def generate_exits(direction_from) # direciton_from should be a key
      exits = []
      keys = [0,1,2,3] # replace with class variable of possible direcitons

      # grab direction from and use logic of 0=2, 2=0, 1=3, 3=1
      
      origin_exit = [2,3,0,1,5,4].delete_at(direction_from)
      exits << keys.delete_at(origin_exit)
      possible_exits = (0..rand(0..keys.length-1))
      
      possible_exits.each do |exit|
        exits << keys.delete_at(rand(0..keys.length-1))
      end
      
      exits.sort

    end

    def generate_loot
      []
    end

    def generate_enemies
      []
    end

    def check_for_person
    end
    
    def self.serial_directions
      {0 => :north, 1 => :east, 2 => :south, 3 => :west, 4 => :up, 5 => :down }
    end
    
    def self.directions
      {north: 0, east: 1, south: 2, west: 3, up: 4, down: 5}
    end

    

  end
end