module Components
  class Player
    include Modules::Health
    
    attr_accessor :name, :inventory

    def initialize(props)
      global_items = props[:global_items]
      @name = props[:name]
      @quests = []
      byebug
      @inventory = Components::Loot.new({global_items: global_items})
    end

    def quests
      @quests << Components::Quest.new({first_game_tile: true})
    end
    
  end
end