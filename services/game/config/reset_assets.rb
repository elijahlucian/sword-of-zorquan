require 'nokogiri'
require 'byebug'
require 'yaml'

def empty_check(val)
  if val.empty?
    return nil
  else 
    return val
  end
end

def parse_xml(xml_doc, key)
  
  return_object = {}

  xml_doc.keys.each do |doc_key| 
    pp "creating #{doc_key} #{key}"
    
    return_object[doc_key] = []
    if key == :classes
      item_list = xml_doc[doc_key].xpath("//#{key[0..-3]}")
    else
      item_list = xml_doc[doc_key].xpath("//#{key[0..-2]}")
    end

    item_list.each do |item, i|
      
      item_value = item.css(:value).text
      item_name = item.at_css(:name).text
      source = nil
      
      if item_name.include?("GP")
        item_name = item_name.split(' - ')[1]
      end
      
      item_text = item.css(:text).text
      if item_text.include? "Source:"
        source = item_text.split('Source:')[-1].split(', ')
        item_text = item_text.split('Source:')[0]
      end
  

      item_traits = nil
      item_actions = nil
      if key == :races || key == :monsters
        # get traits (traits some link table? ugggghh)
        item_traits = []
        item_actions = []
        item_text = empty_check(item.css(:description).text)
        item.css(:trait).each do |t|
          item_traits << {
            name: t.css(:name).text,
            text: t.css(:text).text,
            attack: empty_check(t.css(:attack).text),
          }.compact
        end
        item.css(:action).each do |a|
          item_actions << {
            name: a.css(:name).text,
            text: a.css(:text).text,
            attack: a.css(:attack).text
          }.compact
        end
      end

      # class method

      item_levels = nil
      if key == :classes
        item_text = nil

        item_levels = {}

        item.css(:autolevel).each_with_index do |level, i|
          current_level = {}
          current_level[:features] = []
          current_level[:slots] = level.css(:slots).text.split(',')

          level.css(:feature).each do |f|
            feature = {
              name: f.css(:name).text,
              optional: f.xpath("@optional").first&.value == "YES",
              text: f.css(:text).text
            }

            current_level[:features] << feature
          end
          item_levels[level.attributes['level'].value] = current_level
        end
      end
      
      # modifiers to array, possible shoud have own subcategory

      modifier_values = empty_check(item.css(:modifier).text)
      
      if modifier_values
        modifier_values = []
        item.css(:modifier).each do |m|
          modifier_values << m.text
        end
      end

      return_object[doc_key] << {
        name: item_name,
        value: item_value.empty? ? 0 : item_value,
        weight: empty_check(item.css(:weight).text),
        magic: empty_check(item.css(:magic).text),
        rarity: empty_check(item.css(:rarity).text),
        type: empty_check(item.css(:type).text),
        size: empty_check(item.css(:size).text),
        level: empty_check(item.css(:level).text),
        damage_primary: empty_check(item.css(:dmg1).text),
        damage_secondary: empty_check(item.css(:dmg2).text),
        damage_type: empty_check(item.css(:dmgType).text),
        modifier_type: item.xpath("modifier/@category").first&.value,
        modifier_values: modifier_values,
        time: empty_check(item.css(:time).text),
        school: empty_check(item.css(:school).text),
        range: empty_check(item.css(:range).text),
        role: empty_check(item.css(:role).text),
        ability: empty_check(item.css(:ability).text),
        speed: empty_check(item.css(:speed).text),
        alignment: empty_check(item.css(:alignment).text),
        hd: empty_check(item.css(:hd).text),
        ac: empty_check(item.css(:ac).text),
        hp: empty_check(item.css(:hp).text),
        str: empty_check(item.css(:str).text),
        dex: empty_check(item.css(:dex).text),
        con: empty_check(item.css(:con).text),
        int: empty_check(item.css(:int).text),
        wis: empty_check(item.css(:wis).text),
        cha: empty_check(item.css(:cha).text),
        skill: empty_check(item.css(:skill).text),
        cr: empty_check(item.css(:cr).text),
        prerequisite: empty_check(item.css(:prerequisite).text),
        passive: empty_check(item.css(:passive).text),

        # arrays
        components: empty_check(item.css(:components).text.split(', ')),
        immune_to: empty_check(item.css(:immune).text.split(', ')),
        condition_immune: empty_check(item.css(:conditionImmune).text.split(', ')),
        properties: empty_check(item.css(:property).text.split(', ')),
        languages: empty_check(item.css(:languages).text.split(', ')),
        proficiencies: empty_check(item.css(:proficiency).text.split(', ')),
        senses: empty_check(item.css(:senses).text.split(', ')),
        spells: empty_check(item.css(:spells).text.split(', ')),
        abilities: empty_check(item.css(:spellAbility).text.split(', ')),
        classes: empty_check(item.css(:classes).text.split(', ')),

        # precalculated
        levels: item_levels,
        traits: item_traits,
        actions: item_actions,
        source: source,
        text: item_text
    }.compact

    # print ".DONE! \n"
    end
  end
  return_object
end

def get_xmls_by_folder(key)
  
  xml_doc = {}  
  Dir["./lib/lore/#{key}/*.xml"].each do |file| 
  # puts "importing #{file}"
    "./lib/lore/#{key}/#{File.basename(file, '.xml')}"
    file_key = file.split('/')[-1].split('_')[0].downcase.to_sym
    xml_doc[file_key] = File.open(file) { |f| Nokogiri::XML(f) }
  end
  # puts "Creating #{key} of type #{xml_doc.keys}"
  parse_xml(xml_doc, key)

end  


total_assets = 0
keys = [:items, :spells, :races, :monsters, :feats, :classes]
# keys = [:items]
keys.each do |k|
  puts "","Importing #{k} From D&D XML File",""
  puts "CREATING @#{k}"
  instance_variable_set("@#{k}",get_xmls_by_folder(k))
  c = 0 
  instance = instance_variable_get("@#{k}")  
  instance.keys.each do |z|
    c += instance[z].count
  end
  File.open("./lib/lore/swords_of_zorquan_#{k}.yml", "w+") { |f| f.write instance_variable_get("@#{k}").to_yaml }
  puts "Imported #{c} items for #{k}"
  total_assets += c
end
puts 
puts 
puts '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
puts 
puts 
puts "Total assets imported: #{total_assets}"
puts 
puts 
puts '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
puts 
puts 
