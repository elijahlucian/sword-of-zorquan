lib = File.expand_path("../../lib", __FILE__)
$:.unshift(lib) unless $:.include?(lib)


# require game components
puts '***********************'
puts 'LOADING GAME COMPONENTS'
puts '***********************'
puts

Dir["./lib/modules/*.rb"].each do |file| 
  puts "> LOADING ./lib/modules/#{File.basename(file, '.rb')}"
  require "./lib/modules/#{File.basename(file, '.rb')}"
end
Dir["./lib/components/*.rb"].each do |file| 
  puts "> LOADING ./lib/components/#{File.basename(file, '.rb')}"
  require "./lib/components/#{File.basename(file, '.rb')}"
end
puts "> assets loaded"
